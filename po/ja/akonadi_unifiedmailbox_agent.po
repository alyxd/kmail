msgid ""
msgstr ""
"Project-Id-Version: kmail\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-25 01:40+0000\n"
"PO-Revision-Date: 2018-09-14 20:38-0700\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: settingsdialog.cpp:53 settingsdialog.cpp:128
#, kde-format
msgid "Add"
msgstr ""

#: settingsdialog.cpp:68 settingsdialog.cpp:132
#, kde-format
msgid "Modify"
msgstr ""

#: settingsdialog.cpp:90 settingsdialog.cpp:134
#, kde-format
msgid "Remove"
msgstr ""

#: settingsdialog.cpp:100
#, kde-format
msgid "Do you really want to remove unified mailbox <b>%1</b>?"
msgstr ""

#: settingsdialog.cpp:101
#, kde-format
msgctxt "@title:window"
msgid "Really Remove?"
msgstr ""

#: unifiedmailboxagent.cpp:45 unifiedmailboxagent.cpp:135
#, kde-format
msgid "Unified Mailboxes"
msgstr ""

#: unifiedmailboxagent.cpp:168
#, kde-format
msgid "Synchronizing unified mailbox %1"
msgstr ""

#: unifiedmailboxeditor.cpp:85
#, kde-format
msgctxt "@title:window"
msgid "Configure Unified Mailbox"
msgstr ""

#: unifiedmailboxeditor.cpp:97
#, kde-format
msgid "Name:"
msgstr ""

#: unifiedmailboxeditor.cpp:102
#, kde-format
msgid "Pick icon..."
msgstr ""

#: unifiedmailboxeditor.cpp:103
#, kde-format
msgid "Icon:"
msgstr ""

#: unifiedmailboxmanager.cpp:294
#, kde-format
msgid "Inbox"
msgstr ""

#: unifiedmailboxmanager.cpp:301
#, kde-format
msgid "Sent"
msgstr ""

#: unifiedmailboxmanager.cpp:308
#, kde-format
msgid "Drafts"
msgstr ""
